//! A simple program to write some data to an Excel file.

use chrono::{TimeZone, Utc};
use karo::{col_range, index, Workbook};

fn main() -> karo::Result<()> {
    // Create a new workbook.
    let mut workbook = Workbook::new();

    let datetime = Utc.ymd(2016, 12, 12).and_hms(0, 0, 0);
    workbook.set_custom_property_str("Checked by", "Eve")?;
    workbook.set_custom_property_datetime("Date completed", datetime)?;
    workbook.set_custom_property_integer("Document number", 12345)?;
    workbook.set_custom_property_number("Reference number", 1.2345)?;
    workbook.set_custom_property_boolean("Has review", true)?;
    workbook.set_custom_property_boolean("Signed off", false)?;

    {
        // Add a worksheet with a user defined name.
        let worksheet = workbook.add_worksheet(None)?;

        // Widen the first column to make the text clearer.
        worksheet.set_column(col_range(0, 0)?, 50f64, None)?;

        worksheet.write_string(
            index(0, 0)?,
            "Select 'Workbook Properties' to see properties.",
            None,
        )?;
    }

    workbook.write_file("doc_custom_properties.xlsx")?;

    Ok(())
}

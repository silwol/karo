//! A simple program to write some data to an Excel file.

use karo::{index, Format, NumFormat, Workbook};

struct Expense {
    item: &'static str,
    cost: f64,
}

fn main() -> karo::Result<()> {
    let expenses = [
        Expense {
            item: "Rent",
            cost: 1000f64,
        },
        Expense {
            item: "Gas",
            cost: 100f64,
        },
        Expense {
            item: "Food",
            cost: 300f64,
        },
        Expense {
            item: "Gym",
            cost: 50f64,
        },
    ];

    // Create a new workbook.
    let mut workbook = Workbook::new();

    {
        // Add a worksheet with a user defined name.
        let worksheet = workbook.add_worksheet(None)?;

        // Add a bold format to use to highlight cells.
        let mut bold = Format::default();
        bold.font.bold = true;

        // Add a number format for cells with money.
        let mut money = Format::default();
        money.num_format = NumFormat::from_format_string("$#,##0");

        let mut row = 0u32;

        worksheet.write_string(index(row, 0)?, "Item", Some(&bold))?;
        worksheet.write_string(index(row, 1)?, "Cost", Some(&bold))?;
        row += 1;

        for Expense { item, cost } in expenses.iter() {
            worksheet.write_string(index(row, 0)?, item, None)?;
            worksheet.write_number(index(row, 1)?, *cost, Some(&money))?;
            row += 1;
        }

        worksheet.write_string(index(row, 0)?, "Total", Some(&bold))?;
        worksheet.write_formula(
            index(row, 1)?,
            "=SUM(B2:B5)",
            Some(&money),
        )?;
    }

    workbook.write_file("tutorial02.xlsx")?;

    Ok(())
}

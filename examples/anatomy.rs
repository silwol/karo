//! Anatomy of a simple karo program.

use karo::{col_range, index, Format, NumFormat, Workbook};

fn main() -> karo::Result<()> {
    // Create a new workbook.
    let mut workbook = Workbook::new();

    // Add some cell formats.
    let mut myformat1 = Format::default();
    let mut myformat2 = Format::default();

    // Set the bold property for the first format.
    myformat1.font.bold = true;

    // Set a number format for the second format.
    myformat2.num_format = NumFormat::Custom("$#,##0.00".to_string());

    {
        // Add a worksheet with a user defined name.
        let worksheet1 = workbook.add_worksheet(Some("Demo"))?;

        // Widen the first column to make the text clearer.
        worksheet1.set_column(col_range(0, 0)?, 20.0, None)?;

        // Write some unformatted data.
        worksheet1.write_string(index(0, 0)?, "Peach", None)?;
        worksheet1.write_string(index(1, 0)?, "Plum", None)?;

        // Write formatted data.
        worksheet1.write_string(index(2, 0)?, "Pear", Some(&myformat1))?;

        // Formats can be reused.
        worksheet1.write_string(
            index(3, 0)?,
            "Persimmon",
            Some(&myformat1),
        )?;

        // Write some numbers.
        worksheet1.write_number(index(5, 0)?, 123.0, None)?;
        worksheet1.write_number(
            index(6, 0)?,
            4567.555,
            Some(&myformat2),
        )?;
    }

    {
        let worksheet2 = workbook.add_worksheet(None)?;

        worksheet2.write_string(
            index(0, 0)?,
            "Some text",
            Some(&myformat1),
        )?;
    }

    workbook.write_file("anatomy.xlsx")?;

    Ok(())
}

//! A simple program to write some data to an Excel file.

use karo::{col_range, index, Workbook};

fn main() -> karo::Result<()> {
    // Create a new workbook.
    let mut workbook = Workbook::new();

    // This method returns an &mut DocProperties.
    let p = workbook.properties();
    p.title = "This is an example spreadsheet".to_string();
    p.subject = "With document properties".to_string();
    p.author = "Max Mustermann".to_string();
    p.manager = "Dr. Heinz Doofenshmirtz".to_string();
    p.company = "of Wolves".to_string();
    p.category = "Example spreadsheets".to_string();
    p.keywords = "Sample, Example, Properties".to_string();
    p.comments = "Created with karo".to_string();
    p.status = "Quo".to_string();

    {
        // Add a worksheet with a user defined name.
        let worksheet = workbook.add_worksheet(None)?;

        // Widen the first column to make the text clearer.
        worksheet.set_column(col_range(0, 0)?, 50f64, None)?;

        worksheet.write_string(
            index(0, 0)?,
            "Select 'Workbook Properties' to see properties.",
            None,
        )?;
    }

    workbook.write_file("doc_properties.xlsx")?;

    Ok(())
}

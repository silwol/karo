use karo::{index, Workbook};

fn main() -> karo::Result<()> {
    let mut workbook = Workbook::new();

    let worksheet = workbook.add_worksheet(None)?;

    worksheet.write_string(index(0, 0)?, "Hello", None)?;
    worksheet.write_number(index(1, 0)?, 123.0, None).unwrap();

    workbook.write_file("hello_world.xlsx")?;

    Ok(())
}

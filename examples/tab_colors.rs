use karo::{colors, index, Workbook};
use rgb::RGB8;

fn main() -> karo::Result<()> {
    let mut workbook = Workbook::new();

    {
        let worksheet1 = workbook.add_worksheet(None)?;
        worksheet1.set_tab_color(colors::RED);
    }
    {
        let worksheet2 = workbook.add_worksheet(None)?;
        worksheet2.set_tab_color(colors::GREEN);
    }
    {
        let worksheet3 = workbook.add_worksheet(None)?;
        worksheet3.set_tab_color(RGB8 {
            r: 0xff,
            g: 0x99,
            b: 0x00,
        });
    }
    {
        let worksheet4 = workbook.add_worksheet(None)?;
        worksheet4.write_string(index(0, 0)?, "Hello", None)?;
    }

    workbook.write_file("tab_colors.xlsx")?;

    Ok(())
}

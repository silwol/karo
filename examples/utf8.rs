use karo::{index, Workbook};

fn main() -> karo::Result<()> {
    let mut workbook = Workbook::new();

    {
        let worksheet = workbook.add_worksheet(None)?;
        worksheet.write_string(
            index(2, 1)?,
            "Это фраза на русском!",
            None,
        )?;
    }

    workbook.write_file("utf8.xlsx")?;

    Ok(())
}

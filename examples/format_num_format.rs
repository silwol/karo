//! Example of writing some data with numeric formatting to a simple
//! Excel file.

use karo::{col_range, index, Format, NumFormat, Workbook};

fn main() -> karo::Result<()> {
    // Create a new workbook.
    let mut workbook = Workbook::new();

    {
        let worksheet = workbook.add_worksheet(None)?;

        // Widen the first column to make the text clearer.
        worksheet.set_column(col_range(0, 0)?, 30f64, None)?;

        let mut f = Format::default();

        // 3.1415926
        worksheet.write_number(index(0, 0)?, 3.1415926, None)?;

        // 3.142
        f.num_format = NumFormat::from_format_string("0.000");
        worksheet.write_number(index(1, 0)?, 3.1415926, Some(&f))?;

        // 1,235
        f.num_format = NumFormat::from_format_string("#,##0");
        worksheet.write_number(index(2, 0)?, 1234.56, Some(&f))?;

        // 1,234.56
        f.num_format = NumFormat::from_format_string("#,##0.00");
        worksheet.write_number(index(3, 0)?, 1234.56, Some(&f))?;

        // 49.99
        f.num_format = NumFormat::from_format_string("0.00");
        worksheet.write_number(index(4, 0)?, 49.99, Some(&f))?;

        // 01/01/01
        f.num_format = NumFormat::from_format_string("mm/dd/yy");
        worksheet.write_number(index(5, 0)?, 36892.521, Some(&f))?;

        // Jan 1 2001
        f.num_format = NumFormat::from_format_string("mmm d yyyy");
        worksheet.write_number(index(6, 0)?, 36892.521, Some(&f))?;

        // 1 January 2001
        f.num_format = NumFormat::from_format_string("d mmmm yyyy");
        worksheet.write_number(index(7, 0)?, 36892.521, Some(&f))?;

        // 01/01/2001 12:30 AM
        f.num_format =
            NumFormat::from_format_string("dd/mm/yyyy hh:mm AM/PM");
        worksheet.write_number(index(8, 0)?, 36892.521, Some(&f))?;

        // 1 dollar and .87 cents
        f.num_format = NumFormat::from_format_string(
            "0 \"dollar and\" .00 \"cents\"",
        );
        worksheet.write_number(index(9, 0)?, 1.87, Some(&f))?;

        // Show limited conditional number formats.
        f.num_format = NumFormat::from_format_string(
            "[Green]General;[Red]-General;General",
        );
        worksheet.write_number(index(10, 0)?, 123.0, Some(&f))?; // > 0 Green
        worksheet.write_number(index(11, 0)?, -45.0, Some(&f))?; // < 0 Red
        worksheet.write_number(index(12, 0)?, 0.0, Some(&f))?; // = 0 Default color

        // Format a Zip code
        f.num_format = NumFormat::from_format_string("00000");
        worksheet.write_number(index(13, 0)?, 1209.0, Some(&f))?;
    }

    workbook.write_file("format_num_format.xlsx")?;

    Ok(())
}

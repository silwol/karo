//! A simple program to write some data to an Excel file.

use karo::{index, Workbook};

struct Expense {
    item: &'static str,
    cost: f64,
}

fn main() -> karo::Result<()> {
    let expenses = [
        Expense {
            item: "Rent",
            cost: 1000f64,
        },
        Expense {
            item: "Gas",
            cost: 100f64,
        },
        Expense {
            item: "Food",
            cost: 300f64,
        },
        Expense {
            item: "Gym",
            cost: 50f64,
        },
    ];

    // Create a new workbook.
    let mut workbook = Workbook::new();

    {
        // Add a worksheet with a user defined name.
        let worksheet = workbook.add_worksheet(None)?;

        let mut row = 0u32;
        for Expense { item, cost } in expenses.iter() {
            worksheet.write_string(index(row, 0)?, item, None)?;
            worksheet.write_number(index(row, 1)?, *cost, None)?;
            row += 1;
        }

        worksheet.write_string(index(row, 0)?, "Total", None)?;
        worksheet.write_formula(index(row, 1)?, "=SUM(B1:B4)", None)?;
    }

    workbook.write_file("tutorial01.xlsx")?;

    Ok(())
}

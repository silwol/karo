//! Example of writing some data with font formatting.

use karo::{col_range, index, Format, Workbook};

fn main() -> karo::Result<()> {
    // Create a new workbook.
    let mut workbook = Workbook::new();

    // Add some cell formats.
    let mut myformat1 = Format::default();
    let mut myformat2 = Format::default();
    let mut myformat3 = Format::default();

    // Set the bold property for format 1.
    myformat1.font.bold = true;

    // Set the italic property for format 2.
    myformat2.font.italic = true;

    // Set the bold and italic properties for format 3.
    myformat3.font.bold = true;
    myformat3.font.italic = true;

    {
        let worksheet = workbook.add_worksheet(None)?;

        // Widen the first column to make the text clearer.
        worksheet.set_column(col_range(0, 0)?, 20f64, None)?;

        worksheet.write_string(
            index(0, 0)?,
            "This is bold",
            Some(&myformat1),
        )?;
        worksheet.write_string(
            index(1, 0)?,
            "This is italic",
            Some(&myformat2),
        )?;
        worksheet.write_string(
            index(2, 0)?,
            "Bold and italic",
            Some(&myformat3),
        )?;
    }

    workbook.write_file("format_font.xlsx")?;

    Ok(())
}

pub struct Chart {}

pub enum ChartType {
    Area,
    AreaStacked,
    AreaStackedPercent,
    Bar,
    BarStacked,
    BarStackedPercent,
    Column,
    ColumnStacked,
    ColumnStackedPercent,
    Doughnut,
    Line,
    Pie,
    Scatter,
    ScatterStraight,
    ScatterStraightWithMarkers,
    Smooth,
    SmoothWithMarkers,
    Radar,
    RadarWithMarkers,
    RadarFilled,
}

use super::*;

/// Test assembling a complete file.
#[test]
fn content_types01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <Types xmlns=\"http://schemas.openxmlformats.org/package/2006/content-types\">\
          \
          <Default Extension=\"rels\" ContentType=\"application/vnd.openxmlformats-package.relationships+xml\"/>\
          <Default Extension=\"xml\" ContentType=\"application/xml\"/>\
          <Default Extension=\"jpeg\" ContentType=\"image/jpeg\"/>\
          \
          <Override PartName=\"/docProps/app.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.extended-properties+xml\"/>\
          <Override PartName=\"/docProps/core.xml\" ContentType=\"application/vnd.openxmlformats-package.core-properties+xml\"/>\
          <Override PartName=\"/xl/styles.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.styles+xml\"/>\
          <Override PartName=\"/xl/theme/theme1.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.theme+xml\"/>\
          <Override PartName=\"/xl/workbook.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet.main+xml\"/>\
          <Override PartName=\"/xl/worksheets/sheet1.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.worksheet+xml\"/>\
          <Override PartName=\"/xl/sharedStrings.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.sharedStrings+xml\"/>\
          <Override PartName=\"/xl/calcChain.xml\" ContentType=\"application/vnd.openxmlformats-officedocument.spreadsheetml.calcChain+xml\"/>\
        </Types>\
        ";

    let mut content_types = ContentTypes::default();
    content_types.add_override(
        "/xl/workbook.xml",
        format!("{}spreadsheetml.sheet.main+xml", APP_DOCUMENT),
    );
    content_types.add_worksheet_name("/xl/worksheets/sheet1.xml");
    content_types.add_default("jpeg", "image/jpeg");
    content_types.add_shared_strings();
    content_types.add_calc_chain();

    assert_eq!(
        content_types.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

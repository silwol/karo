use super::*;
use crate::index;

#[test]
fn array_formula_01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"A1:C7\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1\" spans=\"1:3\">\
              <c r=\"A1\">\
                <f t=\"array\" ref=\"A1\">SUM(B1:C1*B2:C2)</f>\
                <v>9500</v>\
              </c>\
              <c r=\"B1\">\
                <v>500</v>\
              </c>\
              <c r=\"C1\">\
                <v>300</v>\
              </c>\
            </row>\
            <row r=\"2\" spans=\"1:3\">\
              <c r=\"A2\">\
                <f t=\"array\" ref=\"A2\">SUM(B1:C1*B2:C2)</f>\
                <v>9500</v>\
              </c>\
              <c r=\"B2\">\
                <v>10</v>\
              </c>\
              <c r=\"C2\">\
                <v>15</v>\
              </c>\
            </row>\
            <row r=\"5\" spans=\"1:3\">\
              <c r=\"A5\">\
                <f t=\"array\" ref=\"A5:A7\">TREND(C5:C7,B5:B7)</f>\
                <v>22196</v>\
              </c>\
              <c r=\"B5\">\
                <v>1</v>\
              </c>\
              <c r=\"C5\">\
                <v>20234</v>\
              </c>\
            </row>\
            <row r=\"6\" spans=\"1:3\">\
              <c r=\"A6\">\
                <v>0</v>\
              </c>\
              <c r=\"B6\">\
                <v>2</v>\
              </c>\
              <c r=\"C6\">\
                <v>21003</v>\
              </c>\
            </row>\
            <row r=\"7\" spans=\"1:3\">\
              <c r=\"A7\">\
                <v>0</v>\
              </c>\
              <c r=\"B7\">\
                <v>3</v>\
              </c>\
              <c r=\"C7\">\
                <v>10000</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );

    worksheet.write_array_formula_num(
        index(0, 0)?,
        index(0, 0)?,
        "{=SUM(B1:C1*B2:C2)}",
        None,
        9500f64,
    )?;
    worksheet.write_array_formula_num(
        index(1, 0)?,
        index(1, 0)?,
        "{=SUM(B1:C1*B2:C2)}",
        None,
        9500f64,
    )?;
    worksheet.write_array_formula_num(
        index(4, 0)?,
        index(6, 0)?,
        "{=TREND(C5:C7*B5:C7)}",
        None,
        22196f64,
    )?;

    worksheet.write_number(index(0, 1)?, 500f64, None)?;
    worksheet.write_number(index(1, 1)?, 10f64, None)?;
    worksheet.write_number(index(4, 1)?, 1f64, None)?;
    worksheet.write_number(index(5, 1)?, 2f64, None)?;
    worksheet.write_number(index(6, 1)?, 3f64, None)?;

    worksheet.write_number(index(0, 2)?, 300f64, None)?;
    worksheet.write_number(index(1, 2)?, 15f64, None)?;
    worksheet.write_number(index(4, 2)?, 20234f64, None)?;
    worksheet.write_number(index(5, 2)?, 21003f64, None)?;
    worksheet.write_number(index(6, 2)?, 10000f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

use super::*;
use crate::index;

#[test]
fn spans01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"B3\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"3\" spans=\"2:2\">\
              <c r=\"B3\">\
                <v>2000</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );
    worksheet.write_number(index(2, 1)?, 2000f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

#[test]
fn spans02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"A1048576\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1048576\" spans=\"1:1\">\
              <c r=\"A1048576\">\
                <v>123</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let sheet_index = 0usize;
    let shared_strings = SharedStrings::default();
    let formats = Formats::default();
    let workbook_sheet_properties = WorkbookSheetProperties::default();

    let mut worksheet = Worksheet::new(
        sheet_index,
        shared_strings,
        formats,
        workbook_sheet_properties,
    );
    worksheet.write_number(index(1048575, 0)?, 123f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

#[test]
fn spans03() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"XFD1\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1\" spans=\"16384:16384\">\
              <c r=\"XFD1\">\
                <v>123</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );
    worksheet.write_number(index(0, 16383)?, 123f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

#[test]
fn spans04() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"XFD1048576\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1048576\" spans=\"16384:16384\">\
              <c r=\"XFD1048576\">\
                <v>123</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );
    worksheet.write_number(index(1048575, 16383)?, 123f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

#[test]
fn spans05() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"A1:T20\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1\" spans=\"1:16\">\
              <c r=\"A1\">\
                <v>1</v>\
              </c>\
            </row>\
            <row r=\"2\" spans=\"1:16\">\
              <c r=\"B2\">\
                <v>2</v>\
              </c>\
            </row>\
            <row r=\"3\" spans=\"1:16\">\
              <c r=\"C3\">\
                <v>3</v>\
              </c>\
            </row>\
            <row r=\"4\" spans=\"1:16\">\
              <c r=\"D4\">\
                <v>4</v>\
              </c>\
            </row>\
            <row r=\"5\" spans=\"1:16\">\
              <c r=\"E5\">\
                <v>5</v>\
              </c>\
            </row>\
            <row r=\"6\" spans=\"1:16\">\
              <c r=\"F6\">\
                <v>6</v>\
              </c>\
            </row>\
            <row r=\"7\" spans=\"1:16\">\
              <c r=\"G7\">\
                <v>7</v>\
              </c>\
            </row>\
            <row r=\"8\" spans=\"1:16\">\
              <c r=\"H8\">\
                <v>8</v>\
              </c>\
            </row>\
            <row r=\"9\" spans=\"1:16\">\
              <c r=\"I9\">\
                <v>9</v>\
              </c>\
            </row>\
            <row r=\"10\" spans=\"1:16\">\
              <c r=\"J10\">\
                <v>10</v>\
              </c>\
            </row>\
            <row r=\"11\" spans=\"1:16\">\
              <c r=\"K11\">\
                <v>11</v>\
              </c>\
            </row>\
            <row r=\"12\" spans=\"1:16\">\
              <c r=\"L12\">\
                <v>12</v>\
              </c>\
            </row>\
            <row r=\"13\" spans=\"1:16\">\
              <c r=\"M13\">\
                <v>13</v>\
              </c>\
            </row>\
            <row r=\"14\" spans=\"1:16\">\
              <c r=\"N14\">\
                <v>14</v>\
              </c>\
            </row>\
            <row r=\"15\" spans=\"1:16\">\
              <c r=\"O15\">\
                <v>15</v>\
              </c>\
            </row>\
            <row r=\"16\" spans=\"1:16\">\
              <c r=\"P16\">\
                <v>16</v>\
              </c>\
            </row>\
            <row r=\"17\" spans=\"17:20\">\
              <c r=\"Q17\">\
                <v>17</v>\
              </c>\
            </row>\
            <row r=\"18\" spans=\"17:20\">\
              <c r=\"R18\">\
                <v>18</v>\
              </c>\
            </row>\
            <row r=\"19\" spans=\"17:20\">\
              <c r=\"S19\">\
                <v>19</v>\
              </c>\
            </row>\
            <row r=\"20\" spans=\"17:20\">\
              <c r=\"T20\">\
                <v>20</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );

    for i in 0..20 {
        worksheet.write_number(
            index(i as u32, i as u16)?,
            (i + 1) as f64,
            None,
        )?;
    }

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

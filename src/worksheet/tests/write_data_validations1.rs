use super::*;
use crate::index;
use chrono::{TimeZone, Utc};
use indexmap::indexset;

#[test]
fn write_data_validations01a() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"greaterThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A1\"><formula1>0</formula1></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::GreaterThan(0)),
        index(0, 0)?,
    );
    validation.ignore_blank = true;
    validation.show_input = true;
    validation.show_error = true;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations01b() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"greaterThan\" sqref=\"A1\"><formula1>0</formula1></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::GreaterThan(0)),
        index(0, 0)?,
    );
    validation.ignore_blank = false;
    validation.show_input = false;
    validation.show_error = false;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations02() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"greaterThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A2\"><formula1>E3</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::IntegerFormula(Criterion::GreaterThan(
            "=E3".to_string(),
        )),
        index(1, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations03() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"decimal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A3\"><formula1>0.1</formula1><formula2>0.5</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Decimal(Criterion::Between(0.1f64, 0.5f64)),
        index(2, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations04() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"list\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A4\"><formula1>\"open,high,close\"</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::List {
            values: indexset! {
                "open".to_string(),
                "high".to_string(),
                "close".to_string(),
            },
            show_dropdown: true,
        },
        index(3, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations05() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"list\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A5\"><formula1>$E$4:$G$4</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::ListFormula {
            formula: "=$E$4:$G$4".to_string(),
            show_dropdown: true,
        },
        index(4, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations06() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"date\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A6\"><formula1>39448</formula1><formula2>39794</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Date(Criterion::Between(
            Utc.ymd(2008, 1, 1).and_hms(0, 0, 0),
            Utc.ymd(2008, 12, 12).and_hms(0, 0, 0),
        )),
        index(5, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations07() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" promptTitle=\"Enter an integer:\" prompt=\"between 1 and 100\" sqref=\"A7\"><formula1>1</formula1><formula2>100</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 100)),
        index(6, 0)?,
    );
    validation.input_title = Some("Enter an integer:".to_string());
    validation.input_message = Some("between 1 and 100".to_string());

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

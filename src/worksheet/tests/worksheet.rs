use super::*;
use crate::index;

#[test]
fn worksheet01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"A1\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData/>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

#[test]
fn worksheet02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"A1\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1\" spans=\"1:1\">\
              <c r=\"A1\">\
                <v>123</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );
    worksheet.write_number(index(0, 0)?, 123f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

#[test]
fn worksheet03() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"A1:E9\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"1\" spans=\"1:5\">\
              <c r=\"A1\" t=\"s\">\
                <v>0</v>\
              </c>\
            </row>\
            <row r=\"2\" spans=\"1:5\">\
              <c r=\"C2\">\
                <v>123</v>\
              </c>\
            </row>\
            <row r=\"4\" spans=\"1:5\">\
              <c r=\"B4\" t=\"s\">\
                <v>1</v>\
              </c>\
            </row>\
            <row r=\"9\" spans=\"1:5\">\
              <c r=\"E9\">\
                <v>890</v>\
              </c>\
            </row>\
          </sheetData>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );
    worksheet.write_string(index(0, 0)?, "Foo", None)?;
    worksheet.write_number(index(1, 2)?, 123f64, None)?;
    worksheet.write_string(index(3, 1)?, "Bar", None)?;
    worksheet.write_number(index(8, 4)?, 890f64, None)?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

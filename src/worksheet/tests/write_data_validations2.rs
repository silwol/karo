use super::*;
use crate::index;
use chrono::TimeZone;
use indexmap::indexset;

#[test]
fn write_data_validations201() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations202() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"notBetween\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::NotBetween(1, 10)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations203() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::EqualTo(1)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations204() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"notEqual\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::NotEqualTo(1)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations205() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"greaterThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::GreaterThan(1)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations206() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"lessThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::LessThan(1)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations207() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"greaterThanOrEqual\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::GreaterThanOrEqualTo(1)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_data_validations208() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" operator=\"lessThanOrEqual\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::LessThanOrEqualTo(1)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Ignore blank off.
#[test]
fn write_data_validations209() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.ignore_blank = false;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Error style == warning.
#[test]
fn write_data_validations210() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" errorStyle=\"warning\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.error_type = ValidationErrorType::Warning;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Error style == information.
#[test]
fn write_data_validations211() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" errorStyle=\"information\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.error_type = ValidationErrorType::Information;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Input title.
#[test]
fn write_data_validations212() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" promptTitle=\"Input title January\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.input_title = Some("Input title January".to_string());

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Input title + input message.
#[test]
fn write_data_validations213() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" promptTitle=\"Input title January\" prompt=\"Input message February\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.input_title = Some("Input title January".to_string());
    validation.input_message = Some("Input message February".to_string());

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Input title + input message + error title.
#[test]
fn write_data_validations214() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" errorTitle=\"Error title March\" promptTitle=\"Input title January\" prompt=\"Input message February\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.input_title = Some("Input title January".to_string());
    validation.input_message = Some("Input message February".to_string());
    validation.error_title = Some("Error title March".to_string());

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Input title + input message + error title + error message.
#[test]
fn write_data_validations215() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" errorTitle=\"Error title March\" error=\"Error message April\" promptTitle=\"Input title January\" prompt=\"Input message February\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.input_title = Some("Input title January".to_string());
    validation.input_message = Some("Input message February".to_string());
    validation.error_title = Some("Error title March".to_string());
    validation.error_message = Some("Error message April".to_string());

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Input title + input message +
/// error title + error message - input message box.
#[test]
fn write_data_validations216() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showErrorMessage=\"1\" errorTitle=\"Error title March\" error=\"Error message April\" promptTitle=\"Input title January\" prompt=\"Input message February\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.input_title = Some("Input title January".to_string());
    validation.input_message = Some("Input message February".to_string());
    validation.error_title = Some("Error title March".to_string());
    validation.error_message = Some("Error message April".to_string());
    validation.show_input = false;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Input title + input message +
/// error title + error message - input message box.
#[test]
fn write_data_validations217() -> Result<()> {
    let expected =
            "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" errorTitle=\"Error title March\" error=\"Error message April\" promptTitle=\"Input title January\" prompt=\"Input message February\" sqref=\"B5\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let mut validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        index(4, 1)?,
    );
    validation.input_title = Some("Input title January".to_string());
    validation.input_message = Some("Input message February".to_string());
    validation.error_title = Some("Error title March".to_string());
    validation.error_message = Some("Error message April".to_string());
    validation.show_input = false;
    validation.show_error = false;

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// 'any' value on its own shouldn't produce a DV record.
#[test]
fn write_data_validations218() -> Result<()> {
    let expected = "";

    let validation = Validation::new(ValidationType::Any, index(4, 1)?);

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Decimal = 1.2345
#[test]
fn write_data_validations219() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"decimal\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>1.2345</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Decimal(Criterion::EqualTo(1.2345f64)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// List = a,bb,ccc
#[test]
fn write_data_validations220() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"list\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>\"a,bb,ccc\"</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::List {
            values: indexset! {
                "a".to_string(),
                "bb".to_string(),
                "ccc".to_string(),
            },
            show_dropdown: true,
        },
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// List = a,bb,ccc; No dropdown
#[test]
fn write_data_validations221() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"list\" allowBlank=\"1\" showDropDown=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>\"a,bb,ccc\"</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::List {
            values: indexset! {
                "a".to_string(),
                "bb".to_string(),
                "ccc".to_string(),
            },
            show_dropdown: false,
        },
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// List = $D$1:$D$5
#[test]
fn write_data_validations222() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"list\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A1\"><formula1>$D$1:$D$5</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::ListFormula {
            formula: "=$D$1:$D$5".to_string(),
            show_dropdown: true,
        },
        index(0, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Date = 39653 (2008-07-24)
#[test]
fn write_data_validations223() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"date\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>39653</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::DateNumber(Criterion::EqualTo(39653)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Date = 2008-07-24
#[test]
fn write_data_validations224() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"date\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>39653</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Date(Criterion::EqualTo(
            Utc.ymd(2008, 7, 24).and_hms(0, 0, 0),
        )),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Date between ranges.
#[test]
fn write_data_validations225() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"date\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>39448</formula1><formula2>39794</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Date(Criterion::Between(
            Utc.ymd(2008, 1, 1).and_hms(0, 0, 0),
            Utc.ymd(2008, 12, 12).and_hms(0, 0, 0),
        )),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Time = 0.5 (12:00:00)
#[test]
fn write_data_validations226() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"time\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>0.5</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::TimeNumber(Criterion::EqualTo(0.5f64)),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Time = 12:00:00
#[test]
fn write_data_validations227() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"time\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>0.5</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Time(Criterion::EqualTo(NaiveTime::from_hms(
            12, 0, 0,
        ))),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Check data validation range.
#[test]
fn write_data_validations228() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"whole\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5:B10\"><formula1>1</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Integer(Criterion::Between(1, 10)),
        IndexRange::new_row_col_row_col(4, 1, 9, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Multiple validations.
#[test]
fn write_data_validations229() -> Result<()> {
    let expected = "<dataValidations count=\"2\"><dataValidation type=\"whole\" operator=\"greaterThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>10</formula1></dataValidation><dataValidation type=\"whole\" operator=\"lessThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"C10\"><formula1>10</formula1></dataValidation></dataValidations>";

    let validations = vec![
        Validation::new(
            ValidationType::Integer(Criterion::GreaterThan(10)),
            index(4, 1)?,
        ),
        Validation::new(
            ValidationType::Integer(Criterion::LessThan(10)),
            index(9, 2)?,
        ),
    ];

    assert_eq!(validations.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// 'any' with an input message should produce a dataValidation record.
#[test]
fn write_data_validations230() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" promptTitle=\"Input title January\" prompt=\"Input message February\" sqref=\"B5\"/></dataValidations>";

    let mut validation =
        Validation::new(ValidationType::Any, index(4, 1)?);
    validation.input_title = Some("Input title January".to_string());
    validation.input_message = Some("Input message February".to_string());

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Length
#[test]
fn write_data_validations231() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"textLength\" operator=\"greaterThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A1\"><formula1>5</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Length(Criterion::GreaterThan(5usize)),
        index(0, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Length
#[test]
fn write_data_validations232() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"textLength\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A1\"><formula1>5</formula1><formula2>10</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Length(Criterion::Between(5usize, 10usize)),
        index(0, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Length formula
#[test]
fn write_data_validations233() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"textLength\" operator=\"greaterThan\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A1\"><formula1>H1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::LengthFormula(Criterion::GreaterThan(
            "=H1".to_string(),
        )),
        index(0, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Length formula
#[test]
fn write_data_validations234() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"textLength\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"A1\"><formula1>H1</formula1><formula2>H2</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::LengthFormula(Criterion::Between(
            "=H1".to_string(),
            "=H2".to_string(),
        )),
        index(0, 0)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Date equal to formula.
#[test]
fn write_data_validations235() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"date\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>H1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::DateFormula(Criterion::EqualTo("=H1".to_string())),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Date between formula ranges.
#[test]
fn write_data_validations236() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"date\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>H1</formula1><formula2>H2</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::DateFormula(Criterion::Between(
            "=H1".to_string(),
            "=H2".to_string(),
        )),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Time between ranges.
#[test]
fn write_data_validations237() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"time\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>0</formula1><formula2>0.5</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::Time(Criterion::Between(
            NaiveTime::from_hms(0, 0, 0),
            NaiveTime::from_hms(12, 0, 0),
        )),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Time formula.
#[test]
fn write_data_validations238() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"time\" operator=\"equal\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>H1</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::TimeFormula(Criterion::EqualTo("=H1".to_string())),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Time formula.
#[test]
fn write_data_validations239() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"time\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>H1</formula1><formula2>H2</formula2></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::TimeFormula(Criterion::Between(
            "=H1".to_string(),
            "=H2".to_string(),
        )),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

/// Custom formula.
#[test]
fn write_data_validations240() -> Result<()> {
    let expected = "<dataValidations count=\"1\"><dataValidation type=\"custom\" allowBlank=\"1\" showInputMessage=\"1\" showErrorMessage=\"1\" sqref=\"B5\"><formula1>10</formula1></dataValidation></dataValidations>";

    let validation = Validation::new(
        ValidationType::CustomFormula("10".to_string()),
        index(4, 1)?,
    );

    assert_eq!(vec![validation].write_xml_to_string()?.as_str(), expected);

    Ok(())
}

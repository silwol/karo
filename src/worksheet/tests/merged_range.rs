use super::*;

#[test]
fn merged_range01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <worksheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:r=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships\">\
          <dimension ref=\"B3:C3\"/>\
          <sheetViews>\
            <sheetView tabSelected=\"1\" workbookViewId=\"0\"/>\
          </sheetViews>\
          <sheetFormatPr defaultRowHeight=\"15\"/>\
          <sheetData>\
            <row r=\"3\" spans=\"2:3\">\
              <c r=\"B3\" s=\"1\" t=\"s\">\
                <v>0</v>\
              </c>\
              <c r=\"C3\" s=\"1\"/>\
            </row>\
          </sheetData>\
          <mergeCells count=\"1\">\
            <mergeCell ref=\"B3:C3\"/>\
          </mergeCells>\
          <pageMargins left=\"0.7\" right=\"0.7\" top=\"0.75\" bottom=\"0.75\" header=\"0.3\" footer=\"0.3\"/>\
        </worksheet>\
        ";

    let mut worksheet = Worksheet::new(
        0usize,
        Default::default(),
        Default::default(),
        Default::default(),
    );

    let format = Format::default();
    worksheet.merge_range_str(
        IndexRange::new_row_col_row_col(2, 1, 2, 2)?,
        "Foo",
        Some(&format),
    )?;

    assert_eq!(
        worksheet.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

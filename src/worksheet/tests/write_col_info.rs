use super::*;
use crate::col_range;

#[test]
fn write_col_info01() -> Result<()> {
    let expected =
        "<col min=\"2\" max=\"4\" width=\"5.7109375\" customWidth=\"1\"/>";

    let column = Column {
        range: col_range(1, 3)?,
        options: Default::default(),
        width: 5f64,
        format_index: None,
    };

    assert_eq!(column.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_col_info02() -> Result<()> {
    let expected = "<col min=\"6\" max=\"6\" width=\"8.7109375\" hidden=\"1\" customWidth=\"1\"/>";

    let column = Column {
        range: col_range(5, 5)?,
        options: Some(RowColOptions {
            level: Default::default(),
            hidden: true,
            collapsed: false,
        }),
        width: 8f64,
        format_index: None,
    };

    assert_eq!(column.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_col_info03() -> Result<()> {
    let expected =
        "<col min=\"8\" max=\"8\" width=\"9.140625\" style=\"1\"/>";

    let column = Column {
        range: col_range(7, 7)?,
        options: Some(RowColOptions {
            level: Default::default(),
            hidden: false,
            collapsed: false,
        }),
        width: DEFAULT_COL_WIDTH,
        format_index: Some(1usize),
    };

    assert_eq!(column.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_col_info04() -> Result<()> {
    let expected =
        "<col min=\"9\" max=\"9\" width=\"9.140625\" style=\"1\"/>";

    let column = Column {
        range: col_range(8, 8)?,
        options: Some(RowColOptions {
            level: Default::default(),
            hidden: false,
            collapsed: false,
        }),
        width: DEFAULT_COL_WIDTH,
        format_index: Some(1usize),
    };

    assert_eq!(column.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_col_info05() -> Result<()> {
    let expected =
            "<col min=\"10\" max=\"10\" width=\"2.7109375\" customWidth=\"1\"/>";

    let column = Column {
        range: col_range(9, 9)?,
        options: Some(RowColOptions {
            level: Default::default(),
            hidden: false,
            collapsed: false,
        }),
        width: 2f64,
        format_index: None,
    };

    assert_eq!(column.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn write_col_info06() -> Result<()> {
    let expected =
            "<col min=\"12\" max=\"12\" width=\"0\" hidden=\"1\" customWidth=\"1\"/>";

    let column = Column {
        range: col_range(11, 11)?,
        options: Some(RowColOptions {
            level: Default::default(),
            hidden: true,
            collapsed: false,
        }),
        width: DEFAULT_COL_WIDTH,
        format_index: None,
    };

    assert_eq!(column.write_xml_to_string()?.as_str(), expected);

    Ok(())
}

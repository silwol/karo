mod app;
mod borders;
mod cell;
mod chart;
mod chartsheet;
mod content_types;
mod core;
mod error;
mod fills;
mod fonts;
mod format;
mod formats;
mod num_formats;
mod relationships;
mod shared_strings;
mod utility;
mod workbook;
mod workbook_sheet_properties;
mod worksheet;
mod xml;
mod zip_writing;

pub mod colors;

pub type Result<T, E = error::Error> = std::result::Result<T, E>;

pub use borders::{Border, BorderStyle};
pub(crate) use borders::{Borders, CellBorder};
pub(crate) use cell::Cell;
pub use cell::{
    cell, col, col_range, index, range, row, row_range, AsRangeString,
    Col, ColRange, Index, IndexRange, Row, RowRange,
};
pub use chart::{Chart, ChartType};
pub use chartsheet::Chartsheet;
pub(crate) use content_types::ContentTypes;
pub use error::Error;
pub use fills::Pattern;
pub(crate) use fills::{Fill, Fills};
pub(crate) use fonts::{Font, Fonts};
pub use format::{Format, HorizontalAlignment, VerticalAlignment};
pub(crate) use formats::Formats;
pub use num_formats::NumFormat;
pub(crate) use num_formats::NumFormats;
pub(crate) use relationships::Relationships;
pub(crate) use shared_strings::SharedStrings;
pub(crate) use utility::{AsExcelDate, AsPaletteColor, DateMode};
pub use workbook::Workbook;
pub(crate) use workbook_sheet_properties::WorkbookSheetProperties;
pub use worksheet::{
    Criterion, Validation, ValidationErrorType, ValidationType, Worksheet,
};
pub(crate) use xml::{XmlWritable, XmlWriter};
pub(crate) use zip_writing::WriteZip;

const SCHEMA_MS: &str =
    "http://schemas.microsoft.com/office/2006/relationships";
const SCHEMA_OFFICEDOC: &str =
    "http://schemas.openxmlformats.org/officeDocument/2006";
const SCHEMA_DOCUMENT: &str =
    "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
const SCHEMA_PACKAGE: &str =
    "http://schemas.openxmlformats.org/package/2006/relationships";

use super::*;

#[test]
fn styles01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <styleSheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">\
          <fonts count=\"1\">\
            <font>\
              <sz val=\"11\"/>\
               <color theme=\"1\"/>\
               <name val=\"Calibri\"/>\
               <family val=\"2\"/>\
               <scheme val=\"minor\"/>\
             </font>\
           </fonts>\
           <fills count=\"2\">\
             <fill>\
               <patternFill patternType=\"none\"/>\
             </fill>\
             <fill>\
               <patternFill patternType=\"gray125\"/>\
             </fill>\
           </fills>\
           <borders count=\"1\">\
             <border>\
               <left/>\
               <right/>\
               <top/>\
               <bottom/>\
               <diagonal/>\
             </border>\
           </borders>\
           <cellStyleXfs count=\"1\">\
             <xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\"/>\
           </cellStyleXfs>\
           <cellXfs count=\"1\">\
             <xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\" xfId=\"0\"/>\
           </cellXfs>\
           <cellStyles count=\"1\">\
             <cellStyle name=\"Normal\" xfId=\"0\" builtinId=\"0\"/>\
           </cellStyles>\
           <dxfs count=\"0\"/>\
           <tableStyles count=\"0\" defaultTableStyle=\"TableStyleMedium9\" defaultPivotStyle=\"PivotStyleLight16\"/>\
         </styleSheet>
        ";

    let mut formats = Formats::default();

    let format = Format::default();
    // TODO: do everything that is necessary for the test to succeed.

    formats.create_or_get_index(Some(&format));

    assert_eq!(formats.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

#[test]
fn styles02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n
        <styleSheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\">\
           <fonts count=\"4\">\
             <font>\
               <sz val=\"11\"/>\
               <color theme=\"1\"/>\
               <name val=\"Calibri\"/>\
               <family val=\"2\"/>\
               <scheme val=\"minor\"/>\
             </font>\
             <font>\
               <b/>\
               <sz val=\"11\"/>\
               <color theme=\"1\"/>\
               <name val=\"Calibri\"/>\
               <family val=\"2\"/>\
               <scheme val=\"minor\"/>\
             </font>\
             <font>\
               <i/>\
               <sz val=\"11\"/>\
               <color theme=\"1\"/>\
               <name val=\"Calibri\"/>\
               <family val=\"2\"/>\
               <scheme val=\"minor\"/>\
             </font>\
             <font>\
               <b/>\
               <i/>\
               <sz val=\"11\"/>\
               <color theme=\"1\"/>\
               <name val=\"Calibri\"/>\
               <family val=\"2\"/>\
               <scheme val=\"minor\"/>\
             </font>\
           </fonts>\
           <fills count=\"2\">\
             <fill>\
               <patternFill patternType=\"none\"/>\
             </fill>\
             <fill>\
               <patternFill patternType=\"gray125\"/>\
             </fill>\
           </fills>\
           <borders count=\"1\">\
             <border>\
               <left/>\
               <right/>\
               <top/>\
               <bottom/>\
               <diagonal/>\
             </border>\
           </borders>\
           <cellStyleXfs count=\"1\">\
             <xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\"/>\
           </cellStyleXfs>\
           <cellXfs count=\"4\">\
             <xf numFmtId=\"0\" fontId=\"0\" fillId=\"0\" borderId=\"0\" xfId=\"0\"/>\
             <xf numFmtId=\"0\" fontId=\"1\" fillId=\"0\" borderId=\"0\" xfId=\"0\" applyFont=\"1\"/>\
             <xf numFmtId=\"0\" fontId=\"2\" fillId=\"0\" borderId=\"0\" xfId=\"0\" applyFont=\"1\"/>\
             <xf numFmtId=\"0\" fontId=\"3\" fillId=\"0\" borderId=\"0\" xfId=\"0\" applyFont=\"1\"/>\
           </cellXfs>\
           <cellStyles count=\"1\">\
             <cellStyle name=\"Normal\" xfId=\"0\" builtinId=\"0\"/>\
           </cellStyles>\
           <dxfs count=\"0\"/>\
           <tableStyles count=\"0\" defaultTableStyle=\"TableStyleMedium9\" defaultPivotStyle=\"PivotStyleLight16\"/>\
         </styleSheet>\
         ";

    let mut formats = Formats::default();

    {
        let format = Format::default();
        // TODO: do everything that is necessary for the test to succeed.
        formats.create_or_get_index(Some(&format));
    }
    {
        let format = Format::default();
        // TODO: do everything that is necessary for the test to succeed.
        formats.create_or_get_index(Some(&format));
    }
    {
        let format = Format::default();
        // TODO: do everything that is necessary for the test to succeed.
        formats.create_or_get_index(Some(&format));
    }
    {
        let format = Format::default();
        // TODO: do everything that is necessary for the test to succeed.
        formats.create_or_get_index(Some(&format));
    }

    assert_eq!(formats.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

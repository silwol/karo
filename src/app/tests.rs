use super::*;
use indexmap::{indexmap, indexset};

/// Test assembling a complete file.
#[test]
fn app01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <Properties xmlns=\"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties\" xmlns:vt=\"http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes\">\
          <Application>Microsoft Excel</Application>\
          <DocSecurity>0</DocSecurity>\
          <ScaleCrop>false</ScaleCrop>\
          <HeadingPairs>\
            <vt:vector size=\"2\" baseType=\"variant\">\
                <vt:variant>\
                    <vt:lpstr>Worksheets</vt:lpstr>\
                </vt:variant>\
                <vt:variant>\
                    <vt:i4>1</vt:i4>\
                </vt:variant>\
            </vt:vector>\
          </HeadingPairs>\
          <TitlesOfParts>\
            <vt:vector size=\"1\" baseType=\"lpstr\">\
                <vt:lpstr>Sheet1</vt:lpstr>\
            </vt:vector>\
          </TitlesOfParts>\
          <Company></Company>\
          <LinksUpToDate>false</LinksUpToDate>\
          <SharedDoc>false</SharedDoc>\
          <HyperlinksChanged>false</HyperlinksChanged>\
          <AppVersion>12.0000</AppVersion>\
        </Properties>\
        ";

    let part_names = indexset! {
        "Sheet1".to_string(),
    };
    let heading_pairs = indexmap! {
        "Worksheets".to_string() => "1".to_string(),
    };

    let properties = DocProperties::default();

    let app = App {
        part_names,
        heading_pairs,
        properties: &properties,
    };

    assert_eq!(app.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

/// Test assembling a complete file.
#[test]
fn app02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <Properties xmlns=\"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties\" xmlns:vt=\"http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes\">\
          <Application>Microsoft Excel</Application>\
          <DocSecurity>0</DocSecurity>\
          <ScaleCrop>false</ScaleCrop>\
          <HeadingPairs>\
            <vt:vector size=\"2\" baseType=\"variant\">\
                <vt:variant>\
                    <vt:lpstr>Worksheets</vt:lpstr>\
                </vt:variant>\
                <vt:variant>\
                    <vt:i4>2</vt:i4>\
                </vt:variant>\
            </vt:vector>\
          </HeadingPairs>\
          <TitlesOfParts>\
            <vt:vector size=\"2\" baseType=\"lpstr\">\
                <vt:lpstr>Sheet1</vt:lpstr>\
                <vt:lpstr>Sheet2</vt:lpstr>\
            </vt:vector>\
          </TitlesOfParts>\
          <Company></Company>\
          <LinksUpToDate>false</LinksUpToDate>\
          <SharedDoc>false</SharedDoc>\
          <HyperlinksChanged>false</HyperlinksChanged>\
          <AppVersion>12.0000</AppVersion>\
        </Properties>\
        ";

    let part_names = indexset! {
        "Sheet1".to_string(),
        "Sheet2".to_string(),
    };
    let heading_pairs = indexmap! {
        "Worksheets".to_string() => "2".to_string(),
    };

    let properties = DocProperties::default();

    let app = App {
        part_names,
        heading_pairs,
        properties: &properties,
    };

    assert_eq!(app.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

/// Test assembling a complete file.
#[test]
fn app03() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <Properties xmlns=\"http://schemas.openxmlformats.org/officeDocument/2006/extended-properties\" xmlns:vt=\"http://schemas.openxmlformats.org/officeDocument/2006/docPropsVTypes\">\
          <Application>Microsoft Excel</Application>\
          <DocSecurity>0</DocSecurity>\
          <ScaleCrop>false</ScaleCrop>\
          <HeadingPairs>\
            <vt:vector size=\"4\" baseType=\"variant\">\
                <vt:variant>\
                    <vt:lpstr>Worksheets</vt:lpstr>\
                </vt:variant>\
                <vt:variant>\
                    <vt:i4>1</vt:i4>\
                </vt:variant>\
                <vt:variant>\
                    <vt:lpstr>Named Ranges</vt:lpstr>\
                </vt:variant>\
                <vt:variant>\
                    <vt:i4>1</vt:i4>\
                </vt:variant>\
            </vt:vector>\
          </HeadingPairs>\
          <TitlesOfParts>\
            <vt:vector size=\"2\" baseType=\"lpstr\">\
                <vt:lpstr>Sheet1</vt:lpstr>\
                <vt:lpstr>Sheet1!Print_Titles</vt:lpstr>\
            </vt:vector>\
          </TitlesOfParts>\
          <Company></Company>\
          <LinksUpToDate>false</LinksUpToDate>\
          <SharedDoc>false</SharedDoc>\
          <HyperlinksChanged>false</HyperlinksChanged>\
          <AppVersion>12.0000</AppVersion>\
        </Properties>\
        ";

    let part_names = indexset! {
        "Sheet1".to_string(),
        "Sheet1!Print_Titles".to_string(),
    };
    let heading_pairs = indexmap! {
        "Worksheets".to_string() => "1".to_string(),
        "Named Ranges".to_string() => "1".to_string(),
    };

    let properties = DocProperties::default();

    let app = App {
        part_names,
        heading_pairs,
        properties: &properties,
    };

    assert_eq!(app.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

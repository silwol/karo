use crate::{error, Result, XmlWritable};
use snafu::ResultExt;
use std::io::{Seek, Write};
use zip::ZipWriter;

pub(crate) trait WriteZip {
    fn write_xml_file<S: Into<String>, X: XmlWritable>(
        &mut self,
        name: S,
        xml: &X,
    ) -> Result<()>;

    fn write_bytes_file<S: Into<String>>(
        &mut self,
        name: S,
        bytes: &[u8],
    ) -> Result<()>;
}

impl<W: Write + Seek> WriteZip for ZipWriter<W> {
    fn write_xml_file<S: Into<String>, X: XmlWritable>(
        &mut self,
        name: S,
        xml: &X,
    ) -> Result<()> {
        self.start_file(name, zip::write::FileOptions::default())
            .context(error::Zip)?;
        xml.write_xml_document(self)
    }

    fn write_bytes_file<S: Into<String>>(
        &mut self,
        name: S,
        bytes: &[u8],
    ) -> Result<()> {
        self.start_file(name, zip::write::FileOptions::default())
            .context(error::Zip)?;
        self.write_all(bytes).context(error::Io)?;
        Ok(())
    }
}

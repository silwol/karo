use super::*;
use chrono::TimeZone;

/// Test assembling a complete file.
#[test]
fn core01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <cp:coreProperties xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dcmitype=\"http://purl.org/dc/dcmitype/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\
          <dc:creator>A User</dc:creator>\
          <cp:lastModifiedBy>A User</cp:lastModifiedBy>\
          <dcterms:created xsi:type=\"dcterms:W3CDTF\">2010-01-01T00:00:00Z</dcterms:created>\
          <dcterms:modified xsi:type=\"dcterms:W3CDTF\">2010-01-01T00:00:00Z</dcterms:modified>\
        </cp:coreProperties>\
        ";

    let mut properties = DocProperties::default();
    properties.author = "A User".to_string();
    properties.created = Some(Utc.ymd(2010, 1, 1).and_hms(0, 0, 0));

    let core = Core {
        properties: &properties,
    };

    assert_eq!(core.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

/// Test assembling a complete file.
#[test]
fn core02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <cp:coreProperties xmlns:cp=\"http://schemas.openxmlformats.org/package/2006/metadata/core-properties\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:dcterms=\"http://purl.org/dc/terms/\" xmlns:dcmitype=\"http://purl.org/dc/dcmitype/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\
          <dc:title>This is an example spreadsheet</dc:title>\
          <dc:subject>With document properties</dc:subject>\
          <dc:creator>A Person</dc:creator>\
          <cp:keywords>Sample, Example, Properties</cp:keywords>\
          <dc:description>Created with karo</dc:description>\
          <cp:lastModifiedBy>A Person</cp:lastModifiedBy>\
          <dcterms:created xsi:type=\"dcterms:W3CDTF\">2011-04-06T19:45:15Z</dcterms:created>\
          <dcterms:modified xsi:type=\"dcterms:W3CDTF\">2011-04-06T19:45:15Z</dcterms:modified>\
          <cp:category>Example spreadsheets</cp:category>\
          <cp:contentStatus>Quo</cp:contentStatus>\
        </cp:coreProperties>\
        ";

    let mut properties = DocProperties::default();
    properties.title = "This is an example spreadsheet".to_string();
    properties.subject = "With document properties".to_string();
    properties.author = "A Person".to_string();
    properties.keywords = "Sample, Example, Properties".to_string();
    properties.comments = "Created with karo".to_string();
    properties.category = "Example spreadsheets".to_string();
    properties.created = Some(Utc.ymd(2011, 4, 6).and_hms(19, 45, 15));
    properties.status = "Quo".to_string();

    let core = Core {
        properties: &properties,
    };

    assert_eq!(core.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

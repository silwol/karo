use snafu::{Backtrace, Snafu};

#[derive(Debug, Snafu)]
#[snafu(visibility(pub))]
pub enum Error {
    #[snafu(display("sheet name {:?} is already in use.", name))]
    SheetNameAlreadyInUse { name: String, backtrace: Backtrace },
    #[snafu(display("maximum string length of {} exceeded", maximum))]
    MaxStringLengthExceeded {
        maximum: usize,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "invalid column range ordering: {} > {}",
        first,
        last
    ))]
    InvalidColumnRangeOrdering {
        first: u16,
        last: u16,
        backtrace: Backtrace,
    },
    #[snafu(display("invalid row range ordering: {} > {}", first, last))]
    InvalidRowRangeOrdering {
        first: u32,
        last: u32,
        backtrace: Backtrace,
    },
    #[snafu(display("error writing XML"))]
    XmlWrite {
        source: quick_xml::Error,
        backtrace: Backtrace,
    },
    #[snafu(display("error with zip"))]
    Zip {
        source: zip::result::ZipError,
        backtrace: Backtrace,
    },
    #[snafu(display("io error"))]
    Io {
        source: std::io::Error,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "column number {} out of range {} to {}",
        number,
        min,
        max
    ))]
    ColNumberOutOfRange {
        number: u16,
        min: u16,
        max: u16,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "row number {} out of range {} to {}",
        number,
        min,
        max
    ))]
    RowNumberOutOfRange {
        number: u32,
        min: u32,
        max: u32,
        backtrace: Backtrace,
    },
    #[snafu(display("angle {} out of range {} to {}", angle, min, max))]
    AngleOutOfRange {
        angle: i16,
        min: i16,
        max: i16,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "font size {} out of range {} to {}",
        size,
        min,
        max
    ))]
    FontSizeOutOfRange {
        size: f64,
        min: f64,
        max: f64,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "outline level {} out of range {} to {}",
        level,
        min,
        max
    ))]
    OutlineLevelOutOfRange {
        min: u8,
        max: u8,
        level: u8,
        backtrace: Backtrace,
    },
    #[snafu(display("error converting to utf-8"))]
    FromUtf8 {
        source: std::string::FromUtf8Error,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "custom property name {:?} length {} out of range {} to {}",
        name,
        size,
        min,
        max
    ))]
    CustomPropertyNameLengthOutOfRange {
        name: String,
        size: usize,
        min: usize,
        max: usize,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "custom property string value {:?} length {} out of range {} to {}",
        value,
        size,
        min,
        max
    ))]
    CustomPropertyStringValueLengthOutOfRange {
        value: String,
        size: usize,
        min: usize,
        max: usize,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "couldn't parse cell {:?}, must be something like \"A3\" \
         or \"XB42\"",
        string
    ))]
    ParseCellError {
        string: String,
        backtrace: Backtrace,
    },
    #[snafu(display(
        "couldn't parse column {:?}, must be something like \"A\" \
         or \"XB\"",
        string
    ))]
    ParseColError {
        string: String,
        backtrace: Backtrace,
    },
}

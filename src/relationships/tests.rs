use super::*;

/// Test assembling a complete file.
#[test]
fn relationships01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\">\
          <Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/worksheet\" Target=\"worksheets/sheet1.xml\"/>\
          <Relationship Id=\"rId2\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/theme\" Target=\"theme/theme1.xml\"/>\
          <Relationship Id=\"rId3\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/styles\" Target=\"styles.xml\"/>\
          <Relationship Id=\"rId4\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/sharedStrings\" Target=\"sharedStrings.xml\"/>\
          <Relationship Id=\"rId5\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/calcChain\" Target=\"calcChain.xml\"/>\
        </Relationships>\
        ";

    let mut relationships = Relationships::default();
    relationships
        .add_document_relationship("/worksheet", "worksheets/sheet1.xml");
    relationships.add_document_relationship("/theme", "theme/theme1.xml");
    relationships.add_document_relationship("/styles", "styles.xml");
    relationships
        .add_document_relationship("/sharedStrings", "sharedStrings.xml");
    relationships.add_document_relationship("/calcChain", "calcChain.xml");

    assert_eq!(
        relationships.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

/// Test assembling a complete file.
#[test]
fn relationships02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <Relationships xmlns=\"http://schemas.openxmlformats.org/package/2006/relationships\">\
          <Relationship Id=\"rId1\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink\" Target=\"www.foo.com\" TargetMode=\"External\"/>\
          <Relationship Id=\"rId2\" Type=\"http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink\" Target=\"link00.xlsx\" TargetMode=\"External\"/>\
        </Relationships>\
        ";

    let mut relationships = Relationships::default();
    relationships.add_worksheet_relationship(
        "/hyperlink",
        "www.foo.com",
        "External",
    );
    relationships.add_worksheet_relationship(
        "/hyperlink",
        "link00.xlsx",
        "External",
    );

    assert_eq!(
        relationships.write_xml_document_to_string()?.as_str(),
        expected
    );

    Ok(())
}

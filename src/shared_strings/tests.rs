use super::*;

/// Test assembling a complete file.
#[test]
fn sst01() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <sst xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" count=\"7\" uniqueCount=\"3\">\
          <si>\
            <t>neptune</t>\
          </si>\
          <si>\
            <t>mars</t>\
          </si>\
          <si>\
            <t>venus</t>\
          </si>\
        </sst>";

    let mut s = SharedStrings::default();
    s.create_or_get_index("neptune", false);
    s.create_or_get_index("neptune", false);
    s.create_or_get_index("neptune", false);
    s.create_or_get_index("mars", false);
    s.create_or_get_index("mars", false);
    s.create_or_get_index("venus", false);
    s.create_or_get_index("venus", false);

    assert_eq!(s.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

/// Test assembling a complete file.
#[test]
fn sst02() -> Result<()> {
    let expected = "\
        <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n\
        <sst xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" count=\"3\" uniqueCount=\"3\">\
          <si>\
            <t>abcdefg</t>\
          </si>\
          <si>\
            <t xml:space=\"preserve\">   abcdefg</t>\
          </si>\
          <si>\
            <t xml:space=\"preserve\">abcdefg   </t>\
          </si>\
        </sst>";

    let mut s = SharedStrings::default();
    s.create_or_get_index("abcdefg", false);
    s.create_or_get_index("   abcdefg", false);
    s.create_or_get_index("abcdefg   ", false);

    assert_eq!(s.write_xml_document_to_string()?.as_str(), expected);

    Ok(())
}

use crate::{error, CellBorder, Fill, Font, NumFormat, Result};

#[derive(Clone, Hash, PartialEq, Eq, Debug)]
pub struct Format {
    pub font: Font,
    pub num_format: NumFormat,
    pub fill: Fill,
    pub borders: CellBorder,

    pub hidden: bool,
    pub horizontal_alignment: Option<HorizontalAlignment>,
    pub vertical_alignment: Option<VerticalAlignment>,
    pub text_wrap: bool,
    rotation: i16,
    pub indent: u8,
    pub shrink: u8,
}

impl Default for Format {
    fn default() -> Self {
        Format {
            font: Default::default(),
            num_format: Default::default(),
            fill: Default::default(),

            hidden: Default::default(),
            horizontal_alignment: Default::default(),
            vertical_alignment: Default::default(),
            text_wrap: Default::default(),
            rotation: Default::default(),
            indent: Default::default(),
            shrink: Default::default(),
            borders: Default::default(),
        }
    }
}

impl Format {
    pub fn set_text_wrap(&mut self) {
        self.text_wrap = true;
    }

    pub fn set_rotation(&mut self, angle: i16) -> Result<()> {
        // Convert user angle to Excel angle.
        match angle {
            angle if angle >= 0 && angle <= 90 => {
                self.rotation = angle;
                Ok(())
            }
            angle if angle >= -90 && angle < 0 => {
                self.rotation = -angle + 90;
                Ok(())
            }
            270 => {
                self.rotation = 255;
                Ok(())
            }
            _ => error::AngleOutOfRange {
                min: -90i16,
                max: 90i16,
                angle,
            }
            .fail(),
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
pub enum HorizontalAlignment {
    Left,
    Center,
    Right,
    Fill,
    Justify,
    CenterAccross,
    Distributed,
}

impl HorizontalAlignment {
    pub(crate) fn xml_identifier(&self) -> &'static str {
        match self {
            HorizontalAlignment::Left => "left",
            HorizontalAlignment::Center => "center",
            HorizontalAlignment::Right => "right",
            HorizontalAlignment::Fill => "fill",
            HorizontalAlignment::Justify => "justify",
            HorizontalAlignment::CenterAccross => "centerContinuous",
            HorizontalAlignment::Distributed => "distributed",
        }
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
pub enum VerticalAlignment {
    Top,
    Bottom,
    Center,
    Justify,
    Distributed,
}

impl VerticalAlignment {
    pub(crate) fn xml_identifier(&self) -> Option<&'static str> {
        match self {
            VerticalAlignment::Top => Some("top"),
            // Bottom is the default
            VerticalAlignment::Bottom => None,
            VerticalAlignment::Center => Some("center"),
            VerticalAlignment::Justify => Some("justify"),
            VerticalAlignment::Distributed => Some("distributed"),
        }
    }
}

use rgb::RGB8;

pub const BLACK: RGB8 = RGB8 {
    r: 0x00,
    g: 0x00,
    b: 0x00,
};

pub const BLUE: RGB8 = RGB8 {
    r: 0x00,
    g: 0x00,
    b: 0xff,
};

pub const BROWN: RGB8 = RGB8 {
    r: 0x80,
    g: 0x00,
    b: 0x00,
};

pub const CYAN: RGB8 = RGB8 {
    r: 0x00,
    g: 0xff,
    b: 0xff,
};

pub const GRAY: RGB8 = RGB8 {
    r: 0x80,
    g: 0x80,
    b: 0x80,
};

pub const GREEN: RGB8 = RGB8 {
    r: 0x00,
    g: 0x80,
    b: 0x00,
};

pub const LIME: RGB8 = RGB8 {
    r: 0x00,
    g: 0xff,
    b: 0x00,
};

pub const MAGENTA: RGB8 = RGB8 {
    r: 0xff,
    g: 0x00,
    b: 0xff,
};

pub const NAVY: RGB8 = RGB8 {
    r: 0x00,
    g: 0x00,
    b: 0x80,
};

pub const ORANGE: RGB8 = RGB8 {
    r: 0xff,
    g: 0x66,
    b: 0x00,
};

pub const PINK: RGB8 = RGB8 {
    r: 0xff,
    g: 0x00,
    b: 0xff,
};

pub const PURPLE: RGB8 = RGB8 {
    r: 0x80,
    g: 0x00,
    b: 0x80,
};

pub const RED: RGB8 = RGB8 {
    r: 0xff,
    g: 0x00,
    b: 0x00,
};

pub const SILVER: RGB8 = RGB8 {
    r: 0xc0,
    g: 0xc0,
    b: 0xc0,
};

pub const WHITE: RGB8 = RGB8 {
    r: 0xff,
    g: 0xff,
    b: 0xff,
};

pub const YELLOW: RGB8 = RGB8 {
    r: 0xff,
    g: 0xff,
    b: 0x00,
};

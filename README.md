# karo spreadsheet export

I currently do not recommend using this crate, it's still under heavy
development and subject to API changes. Feel free to look at the source
code though if you like.

The purpose of this crate is to write spreadsheet files. It takes strong
inspiration from the `libxlsxwriter` C library for implementing the
functionality, but aims to provide a more Rust-idiomatic API. It's
currently in it's earliy stages, and the API is still being fleshed out.
